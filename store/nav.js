export const state = () => ({
    showSideBar: false,
})


export const mutations = {
    onShow(state) {
        state.showSideBar = true;
    },
    onHide(state) {
        state.showSideBar = false;
    },
    onToggle(state) {
        state.showSideBar = !state.showSideBar;
    },

}
